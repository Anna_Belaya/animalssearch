package ua.belaya.animals.user.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ua.belaya.animals.Application;
import ua.belaya.animals.core.exception.DomainNotFoundException;
import ua.belaya.animals.core.exception.DuplicateDomainException;
import ua.belaya.animals.user.domain.User;
import ua.belaya.animals.user.repository.UserJpaRepository;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

/**
 * @author Anna Belaya
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
@WebAppConfiguration
public class UserServiceImplTest {
    @Autowired
    private UserJpaRepository userRepository;
    @Autowired
    private UserService userService;

    private User user;

    @Before
    public void setUp() {
        userRepository.deleteAll();
        user = new User("anna", "0000");
    }

    @Test
    public void given_user_then_when_calling_add_user_added_expected() {
        User currentUser = userService.add(user);

        assertThat(currentUser.getId(), is(notNullValue()));
        assertThat(currentUser.getNickName(), equalTo(user.getNickName()));
    }

    @Test
    public void given_user_to_update_then_when_calling_update_user_updated_expected() {
        User currentUser = userService.add(user);

        assertThat(currentUser.getNickName(), equalTo(user.getNickName()));

        currentUser.setNickName("juju");
        currentUser.setPassword("1111");
        currentUser = userService.update(currentUser.getId(), currentUser);

        assertThat(currentUser.getNickName(), equalTo("juju"));
        assertThat(currentUser.getPassword(), equalTo("1111"));
    }

    @Test
    public void given_index_then_when_calling_get_user_with_right_index_returned_expected() {
        User currentUser = userService.add(user);

        assertThat(currentUser.getNickName(), equalTo(userService.get(currentUser.getId()).getNickName()));
    }

    @Test
    public void given_nick_name_then_when_calling_get_user_with_right_nick_name_returned_expected() {
        User currentUser = userService.add(user);

        assertThat(currentUser.getNickName(), equalTo(userService.getByUsername(currentUser.getNickName()).getNickName()));
    }

    @Test
    public void given_user_in_database_then_when_calling_delete_user_deleted_expected(){
        User currentUser = userService.add(user);

        userService.delete(currentUser.getId());

        assertThat(userRepository.findOne(currentUser.getId()), nullValue());
    }

    @Test(expected = DuplicateDomainException.class)
    public void given_user_with_duplicated_nick_name_then_when_calling_add_DuplicateDomainException_expected() {
        User firstUser = userService.add(user);
        userService.add(new User(firstUser.getNickName(), "1111"));
    }

    @Test(expected = DuplicateDomainException.class)
    public void given_user_with_duplicated_nick_name_then_when_calling_update_DuplicateDomainException_expected() {
        User firstUser = userService.add(user);
        User secondUser = userService.add(new User("juju", "1111"));

        secondUser.setNickName(firstUser.getNickName());
        userService.update(secondUser.getId(),secondUser);
    }

    @Test(expected = DomainNotFoundException.class)
    public void given_user_with_index_which_can_not_be_in_database_then_when_calling_get_DomainNotFoundException_expected() {
        userService.get(-1L);
    }

    @Test(expected = DomainNotFoundException.class)
    public void given_user_with_nick_name_which_can_not_be_in_database_then_when_calling_getByNickName_DomainNotFoundException_expected(){
        userService.getByUsername("juju");
    }

    @Test(expected = DomainNotFoundException.class)
    public void given_user_then_when_calling_delete_user_deleted_expected() {
        User currentUser = userService.add(user);

        userService.delete(currentUser.getId());

        userService.get(currentUser.getId());
    }
}
