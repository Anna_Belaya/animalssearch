package ua.belaya.animals.config.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ua.belaya.animals.user.domain.User;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Anna Belaya
 */
public class SecurityUser extends User implements UserDetails{
    public SecurityUser(User user){
        this.setNickName(user.getNickName());
        this.setPassword(user.getPassword());
        this.setRole(user.getRole());
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority(this.getRole().toString().toUpperCase()));
        return authorities;
    }

    @Override
    public String getUsername() {
        return getNickName();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
