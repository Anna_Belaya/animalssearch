package ua.belaya.animals.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import ua.belaya.animals.user.domain.User;
import ua.belaya.animals.user.repository.UserJpaRepository;

/**
 * @author Anna Belaya
 */

@Component
public class CustomUserDetailService implements UserDetailsService {

    @Autowired
    private UserJpaRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

        User user = userRepository.getUserByNickName(s);

        if (user == null) {
            throw new UsernameNotFoundException(String.format("User '%s' not found", s));
        }

        return new SecurityUser(user);
    }
}
