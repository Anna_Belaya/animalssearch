package ua.belaya.animals.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.belaya.animals.user.domain.User;
import ua.belaya.animals.user.service.UserService;

import javax.websocket.server.PathParam;
import java.util.List;

/**
 * @author Anna Belaya
 */
@RestController
@RequestMapping("/rest/user")
public class UserControllerRest {
    @Autowired
    private UserService userService;

    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<Void> add(@RequestBody User user) {
        userService.add(user);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<User> get(@PathVariable("id") Long id){
        return new ResponseEntity<User>(userService.get(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Void> update(@PathVariable("id") Long id,
                                       @RequestBody User user){
        userService.update(id, user);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable("id") Long id){
        userService.delete(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<List<User>> list(@PathParam("max") int max,
                                           @PathParam("offset") int offset){
        return new ResponseEntity<List<User>>(userService.list(max,offset), HttpStatus.OK);
    }
}
