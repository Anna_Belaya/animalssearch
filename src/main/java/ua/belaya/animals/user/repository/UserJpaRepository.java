package ua.belaya.animals.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ua.belaya.animals.user.domain.User;

import java.util.List;

/**
 * @author Anna Belaya
 */
public interface UserJpaRepository extends JpaRepository<User, Long> {
    User getUserByNickName(String nickName);

    @Query(value = "SELECT * from user LIMIT ?2,?1", nativeQuery = true)
    List<User> list(int max, int offset);
}
