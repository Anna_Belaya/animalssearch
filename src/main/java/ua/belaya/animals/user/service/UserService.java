package ua.belaya.animals.user.service;

import ua.belaya.animals.core.service.DomainService;
import ua.belaya.animals.user.domain.User;

/**
 * @author Anna Belaya
 */
public interface UserService extends DomainService<User>{
    User getByUsername(String username);
}
