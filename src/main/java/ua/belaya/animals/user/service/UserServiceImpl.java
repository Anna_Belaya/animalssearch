package ua.belaya.animals.user.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import ua.belaya.animals.core.domain.Domains;
import ua.belaya.animals.core.exception.DomainNotFoundException;
import ua.belaya.animals.core.exception.DuplicateDomainException;
import ua.belaya.animals.user.domain.User;
import ua.belaya.animals.user.repository.UserJpaRepository;

import java.util.List;

/**
 * @author Anna Belaya
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserJpaRepository userRepository;

    @Override
    public User add(User user) {
        try {
            userRepository.save(user);
        } catch (DataIntegrityViolationException e) {
            throw new DuplicateDomainException(Domains.USER);
        }

        return user;
    }

    @Override
    public User getByUsername(String nickName) {
        User user = userRepository.getUserByNickName(nickName);

        if (user == null) {
            throw new DomainNotFoundException(Domains.USER, nickName);
        }

        return user;
    }

    @Override
    public User get(Long id) {
        User user = userRepository.findOne(id);

        if (user == null) {
            throw new DomainNotFoundException(Domains.USER, id);
        }

        return user;
    }

    @Override
    public User update(Long id, User user) {
        try {
            user.setId(id);
            userRepository.save(user);
        } catch (DataIntegrityViolationException e) {
            throw new DuplicateDomainException(Domains.USER);
        }

        return user;
    }

    @Override
    public void delete(Long id) {
        if (!userRepository.exists(id)) {
            throw new DomainNotFoundException(Domains.USER, id);
        }

        userRepository.delete(id);
    }

    @Override
    public List<User> list(int max, int offset) {
        return userRepository.list(max, offset);
    }
}
