package ua.belaya.animals.core.service;

import ua.belaya.animals.core.domain.Domain;

import java.util.List;

/**
 * @author Anna Belaya
 */
public interface DomainService<T extends Domain>{
    T add(T domain);
    T get(Long id);
    T update(Long id, T domain);
    void delete(Long id);
    List<T> list(int max, int offset);
}
