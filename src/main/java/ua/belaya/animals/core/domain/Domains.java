package ua.belaya.animals.core.domain;

/**
 * @author Anna Belaya
 */
public enum Domains {
    USER("User");

    private String name;

    Domains(String domainName) {
        name = domainName;
    }

    public String getName() {
        return name;
    }
}
