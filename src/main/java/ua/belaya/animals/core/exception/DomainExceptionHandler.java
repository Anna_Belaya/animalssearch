package ua.belaya.animals.core.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * @author Anna Belaya
 */
@ControllerAdvice
public class DomainExceptionHandler {
    @ExceptionHandler(DomainNotFoundException.class)
    public ResponseEntity<Void> handleDomainNotFoundException(DomainNotFoundException e){
        return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(DuplicateDomainException.class)
    public ResponseEntity<Void> handleDuplicateDomainException(DuplicateDomainException e){
        return new ResponseEntity<Void>(HttpStatus.CONFLICT);
    }
}
