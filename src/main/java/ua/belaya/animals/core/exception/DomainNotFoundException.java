package ua.belaya.animals.core.exception;

import ua.belaya.animals.core.domain.Domains;

/**
 * @author Anna Belaya
 */
public class DomainNotFoundException extends RuntimeException {
    public DomainNotFoundException() {
        super("Domain was not found");
    }

    public DomainNotFoundException(Domains domain, Long id) {
        super(domain.getName() + " with id = " + id + " was not found");
    }

    public DomainNotFoundException(Domains domain, String username) {
        super(domain.getName() + " " + username + " was not found");
    }
}
