package ua.belaya.animals.core.exception;

import ua.belaya.animals.core.domain.Domain;
import ua.belaya.animals.core.domain.Domains;

/**
 * @author Anna Belaya
 */
public class DuplicateDomainException extends RuntimeException{
    public DuplicateDomainException(){
        super("Domain is already in database");
    }

    public DuplicateDomainException(Domains domain){
        super(domain.getName() + " is already in database");
    }
}
