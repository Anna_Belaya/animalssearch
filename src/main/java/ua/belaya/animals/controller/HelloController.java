package ua.belaya.animals.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author Anna Belaya
 */
@Controller
@RequestMapping("/")
public class HelloController {

    @RequestMapping("")
    public ModelAndView hello(){
        return new ModelAndView("index");
    }

    @RequestMapping("test")
    public String test() {
        return "test";
    }

}
